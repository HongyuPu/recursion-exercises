public class Recursion {
    public static void main(String[] args) {

        int[] sumMe = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, };
        System.out.printf("Array Sum: %d\n", arraySum(sumMe, 0));

        int[] minMe = { 0, 1, 1, 2, 3, 5, 8, -42, 13, 21, 34, 55, 89, };
        System.out.printf("Array Min: %d\n", arrayMin(minMe, 0));

        String[] amISymmetric =  {
                "You can cage a swallow can't you but you can't swallow a cage can you",
                "I still say cS 1410 is my favorite class"
        };
        for (String test : amISymmetric) {
            String[] words = test.toLowerCase().split(" ");
            System.out.println();
            System.out.println(test);
            System.out.printf("Is word symmetric: %b\n", isWordSymmetric(words, 0, words.length - 1));
        }

        double weights[][] = {
            { 51.18 },
            { 55.90, 131.25 },
            { 69.05, 133.66, 132.82 },
            { 53.43, 139.61, 134.06, 121.63 }
        };
        System.out.println();
        System.out.println("--- Weight Pyramid ---");
        for (int row = 0; row < weights.length; row++) {
            for (int column = 0; column < weights[row].length; column++) {
                double weight = computePyramidWeights(weights, row, column);
                System.out.printf("%.2f ", weight);
            }
            System.out.println();
        }

        char image[][] = {
                {'*','*',' ',' ',' ',' ',' ',' ','*',' '},
                {' ','*',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ',' ',' ',' ','*','*',' ',' '},
                {' ','*',' ',' ','*','*','*',' ',' ',' '},
                {' ','*','*',' ','*',' ','*',' ','*',' '},
                {' ','*','*',' ','*','*','*','*','*','*'},
                {' ',' ',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ','*','*','*',' ',' ','*',' '},
                {' ',' ',' ',' ',' ','*',' ',' ','*',' '}
        };
        int howMany = howManyOrganisms(image);
        System.out.println();
        System.out.println("--- Labeled Organism Image ---");
        for (char[] line : image) {
            for (char item : line) {
                System.out.print(item);
            }
            System.out.println();
        }
        System.out.printf("There are %d organisms in the image.\n", howMany);

    }

    public static boolean isWordSymmetric(String[] words, int start, int end) {
        if(start == end) { //finishing basecase
            return true;
        }

        if (words[start].equalsIgnoreCase(words[end])) { //checks if opposite words are equal
            start++; //increments place of words
            end--;
            return isWordSymmetric(words, start, end);
        }
        else {
            return false; //returns false if conditions aren't met
        }
    }

    public static long arraySum(int[] data, int position) {
        if (position > data.length - 1) { //finishing basecase
            return 0;
        }
        long sum = data[position]; //sum variable to hold sums
        return sum + arraySum(data, position + 1); //repeats through function until base case
    }

    public static int arrayMin(int[] data, int position) {
        if (position == data.length - 1) { //basecase
            return data[position];
        }

        int min = arrayMin(data, position + 1); //min variable

        if (data[position] < min) { //recursion
            return data[position];
        }
        else {
            return min; //returns the min
        }
    }

    public static double computePyramidWeights(double[][] weight, int row, int column) {
        if (row > weight.length - 1 || row < 0) { //basecases for bounds
            return 0;
        }
        if (column > weight[row].length - 1 || column < 0) { //and negative numbers
            return 0;
        }
        if (weight[row].length  == 0) { //basecase for empty array
            return 0;
        }

        double sum = weight[row][column]; //sum of columns

        if (row == 0 && column == 0) {
            return weight[row][column];
        }
        else if (column - 1 < 0) {
            return sum + computePyramidWeights(weight, row - 1, column) / 2; //operation on left edge
        }
        else if (column == row) {
            return sum + computePyramidWeights(weight, row - 1, column - 1) / 2; //operation on right edge
        }
        else {
            return sum + computePyramidWeights(weight, row - 1, column - 1) / 2 + computePyramidWeights(weight, row - 1, column) / 2; //operation on middle
        }
    }

    public static int howManyOrganisms(char[][] image) {
        if (image.length == 0) { //checks for empty array
            return 0;
        }
        int count = 0; //counts organisms
        int ascii = 97; //starting "a" ascii value
        for (int i = 0; i <= image.length - 1; i++) { //iterates through array
            for (int j = 0; j <= image[i].length - 1; j++) {
                if (image[i][j] == '*') { //finds organisms
                    labelOrganism(image, i, j, ascii); //uses method
                    count++; //increments count
                    ascii++; //increments letters
                }
            }
        }
        return count;
    }

    public static char[][] labelOrganism(char[][] image, int row, int column, int ascii) {
        image[row][column] = (char) ascii; //turns the '*' into a letter
        if (image[row].length - 1 >= column + 1 && image[row][column + 1] == '*') { //checks right of the found organism and labels it
            labelOrganism(image, row, column + 1, ascii);
        }

        if (column - 1 >= 0 && image[row][column - 1] == '*') { //checks left of the found organism and labels it
            labelOrganism(image, row, column - 1, ascii);
        }

        if (image.length - 1 >= row + 1 && image[row + 1].length > column && image[row + 1][column] == '*') { //checks below the found organism and labels it
            labelOrganism(image, row + 1, column, ascii);
        }

        if (row - 1 >= 0 && image[row - 1].length > column && image[row - 1][column] == '*') { //checks on top of the found organism and labels it
            labelOrganism(image, row - 1, column, ascii);
        }
        return image; //returns the array with organism marked
    }
}
